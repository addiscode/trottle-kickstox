package main

import (
	"fmt"
	"time"
	"sync"

	"trottle"
)


/**
* Implements trottle.Task interface
*/
type TrottleWorker struct {
	id int
}

func (self *TrottleWorker) DoWork(item interface{}, bury func(*trottle.Task, time.Duration) error, wg *sync.WaitGroup) error {

	wg.Add(1)
	defer wg.Done()

	castItem,_ := item.(string)

	fmt.Println("Done Working on #", castItem)

	return nil
}


func main() {

	// initialize trottle
	taskQueue := trottle.NewChannelQueueProvider(4)

	// initialize worker
	trWorker := TrottleWorker{}

	// burry some tasks to be executed on kick; we can control priority with sleep
	for i:=0; i<100; i++ {
		iData := fmt.Sprintf("Item %d", i)
		taskQueue.Bury(&trottle.Task{Worker: trWorker.DoWork, Item: iData}, time.Second)	
	}

	// kick 10 tasks from backlog to main queue
	go taskQueue.Kick(10, time.Second)
	
	// loop forever
	for {

		taskQueue.Reserve(time.Second)
	}


	fmt.Println("Hello")
}