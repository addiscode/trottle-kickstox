package trottle

import (
	"time"
	"sync"
)


type Task struct {
	Err error
	Item interface{}
	Worker func(interface{}, func(*Task, time.Duration) error, *sync.WaitGroup) error // worker
}


type QueueProvider interface {
	Put(body *Task, duration time.Duration) (id int64, err error)
	Reserve(timeout time.Duration) (int64, []byte, error)
	Bury(*Task, time.Duration) error
	Kick(num int, duration time.Duration) (int, error)
	Delete(id int64) error
	Close()
}