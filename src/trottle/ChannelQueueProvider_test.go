package trottle

import (
	"fmt"
	"sync"
	"testing"
	"errors"
	"time"
	"hermes/market"
)


var chanQP QueueProvider
var mockStore market.MockStore

func fn(item interface{}, bury func(*Task, time.Duration) error, wg *sync.WaitGroup) error {

	wg.Add(1)
	defer wg.Done()

	time.Sleep(time.Second)

	itm, _ := item.(*market.StoreItem)
	mockStore.Reconcile(itm)

	return nil
}

func init() {

	chanQP = NewChannelQueueProvider(100)
	mockStore = market.MockStore{Name:"Mock store test"}
}


func testFail(t *testing.T, err error) {

	if err != nil {
		t.Error(err)
	}	
}

func TestPut(t *testing.T) {


	for i:=0;i<100;i++ {
		_, err := chanQP.Put( &Task{ Worker: fn, Item: &market.StoreItem{SKU: fmt.Sprintf("xxx%d", i)}}, time.Second * 5)
		// go chanQP.Put( &Task{ f: fn, Item: &market.StoreItem{SKU: fmt.Sprintf("xxx%d", i)}}, time.Second * 5)
		testFail(t, err)
	}

	// job should wait ang go thru
	go chanQP.Put( &Task{ Worker: fn, Item: &market.StoreItem{SKU: fmt.Sprintf("xxx%d", 101)}}, time.Second * 6)
}



func TestReserve(t *testing.T) {

	for i:=0;i<10;i++ {
		_,_,err := chanQP.Reserve(time.Second*5)

		testFail(t, err)

		// t.Log(string(job))
	}

	chanQP.Close()

	chanQP.Reserve(time.Second)


	for i:=0;i<92;i++ {
		_,_,err := chanQP.Reserve(time.Second*5)

		testFail(t, err)

		// t.Log(string(job))
	}


}


func TestBackLog(t *testing.T) {

	chanQP = NewChannelQueueProvider(10)

	faulty := func(item interface{}, bury func(*Task, time.Duration) error, wg *sync.WaitGroup) error {

		itm, _ := item.(*market.StoreItem)
		bury(&Task{ Worker: fn, Item: itm}, time.Second * 1)

		t.Log("Faulty burried job, ", itm)
		return errors.New("Shit happen")
	}


	for i:=0;i<10;i++ {

		if i % 2 == 0 {

			
			//faulty
			go chanQP.Put( &Task{ Worker: faulty, Item: &market.StoreItem{SKU: fmt.Sprintf("xxx%d", i)}}, time.Second * 6)
		}

		go chanQP.Put( &Task{ Worker: fn, Item: &market.StoreItem{SKU: fmt.Sprintf("xxx%d", i)}}, time.Second * 6)
	}


	//run
	for i:=0;i<10;i++ {

		go chanQP.Reserve(time.Second)
	}


	count,_ := chanQP.Kick(10, time.Second * 10)

	t.Log("Kicked, ", count)
	if count < 1 {
		testFail(t, errors.New("Expect >1 got "+ string(count) ))
	}

	count,_ = chanQP.Kick(10, time.Second * 10)
	t.Log("Kicked, ", count)
	if count > 1 {
		testFail(t, errors.New("Expect 0 got "+ string(count) ))
	}


}