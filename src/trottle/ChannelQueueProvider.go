package trottle

import (

	"time"
	"sync"
)



type channelQueueProvider struct {
	concurency int
	taskChan chan *Task
	backlogChan chan *Task
	wg *sync.WaitGroup
}

const (
	HEAP_SIZE_MULTIPLIER = 1000
)

func NewChannelQueueProvider(concurency int) *channelQueueProvider {

	var wg sync.WaitGroup

	return &channelQueueProvider{
		concurency: concurency,
		taskChan: make(chan *Task, concurency),
		backlogChan: make(chan *Task, concurency*HEAP_SIZE_MULTIPLIER),
		wg: &wg,
	}
}


func (s *channelQueueProvider) Put(task *Task, duration time.Duration) (id int64, err error) {

	s.wg.Add(1)
	defer s.wg.Done()

	for {

		// fmt.Println("Putting: ", task.Item)
		if len(s.taskChan) < s.concurency {
			s.taskChan <- task

			break
		}

		time.Sleep(duration)
	}

	return 0, nil
}


func (s *channelQueueProvider) Reserve(timeout time.Duration) (int64, []byte, error) {

	if len(s.taskChan) > 0 {

		task := <-s.taskChan
		go task.Worker(task.Item, s.Bury ,s.wg)

		// out := <- s.outChan

		return 0, []byte(""), nil
	}

	time.Sleep(timeout)

	return 0, nil, nil
}


func (s *channelQueueProvider) Bury(task *Task, duration time.Duration) error {

	for {
		// fmt.Println("Putting: ", task.Item)
		if len(s.backlogChan) < s.concurency*HEAP_SIZE_MULTIPLIER {
			s.backlogChan <- task

			break
		}

		time.Sleep(duration)
	}

	return nil
}


func (s *channelQueueProvider) Kick(num int, duration time.Duration) (int, error) {

	time.Sleep(duration)

	count := len(s.backlogChan)

	if count < num { num = count }

	for i:=0;i<num;i++ {

		task := <- s.backlogChan
		s.taskChan <- task
	}

	return num, nil
}

func (s *channelQueueProvider) Delete(id int64) error {

	return nil
}


func (s *channelQueueProvider) Close() {
	s.wg.Wait()

	close(s.taskChan)
}
